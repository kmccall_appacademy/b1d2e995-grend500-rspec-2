require 'byebug'

def measure(num = 1)
  total = (1..num).map do
    start_time = Time.now
    yield
    end_time = Time.now

    end_time - start_time
  end
  total.inject(0, :+) / num
end
